from celery import Celery

app = Celery('first_step',broker="redis://localhost:6379",backend="redis://localhost:6379")

@app.task
def add(x:int,y:int) -> int:
    return x+y 