from celery import Celery

app = Celery('larger_project',
             broker="redis://localhost:6379",
             backend="redis://localhost:6379",
             include=['larger_project.tasks'])

app.conf.update(
    result_expires=3600,
)

app.conf.update(
    task_routes = {
        'larger_project.tasks.add_node2': {'queue': 'hipri'},
    },
)

if __name__ == '__main__':
    app.start()

   