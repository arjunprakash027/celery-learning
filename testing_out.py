from larger_project.tasks import add,mul,add_node2

#testing the normal working of celery
result = add.delay(2,10)
print(result.ready())
print(result.get())

result2 = mul.apply_async((1,7))
print(result2.get())

#testing the wroking of celery signatures
#signatures can be used to chain tasks togather, run tasks later.
result = add.signature((3,5))
print(result.delay().get())

#testing another node
result = add_node2.apply_async((3,89),queue='hipri')
print(result.get())


