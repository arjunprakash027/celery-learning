from celery import group,chain,chord
from larger_project.tasks import add,mul,xsum

#testing out various porimitives present in celery

#grouping
print(group(add.s(i,i) for i in range(5))().get())
print(group(add.s(1,5),mul.s(1,5))().get())
#print(group(mul.s(i,i) for i in range(5))())
#group(<_regen: [larger_project.tasks.mul(0, 0), mul(1, 1)...]>)

#chaining
print(chain(add.s(4, 4) | mul.s(8))().get())

#chord is a group with callbacks
print(chord((add.s(i,i) for i in range(4)),mul.s(2))().get())
